a = int(input("Введите 1е число:"))
b = int(input("Введите 2е число:"))
sign = input("Введите операцию:")

if sign == "+":
    print("A + B = ", a + b)
elif sign == "-":
    print("A - B = ", a - b)
elif sign == "*":
    print("A * B = ", a * b)
elif sign == "/" and b != 0:
    print("A / B = ", a / b)
elif sign == "/" and b == 0:
    print("На ноль делить нельзя")
else:
    print("Введите операцию корректно")
